﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    internal class Lesson3_2
    {
        public static void Number()
        {
            Console.WriteLine("Task 2");
            Console.WriteLine("Please, enter a number");
            int number = Convert.ToInt32(Console.ReadLine());

            if (number >= 0 & number <= 14)
            {
                Console.WriteLine("The number falls within the range [0 14]");
            }
            else if (number >= 15 & number <= 35)
            { 
                Console.WriteLine("The number falls within the range [15 35]");
            }
            else if (number >= 36 & number <= 49)
            {
                Console.WriteLine("The number falls within the range [36 49]");
            }
            else if (number >= 50 & number <= 100)
            {
                Console.WriteLine("The number falls within the range [36 49]");
            }
            else if (number >= 101)
            {
                Console.WriteLine("The number does not fall into any of the ranges");
            }

        }
    }
}
