﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    internal class Lesson3_3
    {
        public static void Translator()
        {
        Console.WriteLine("Task 3");
        Console.WriteLine("Please enter a word");
        string word = Console.ReadLine();
       
        switch (word)
        {
            case "дождь":
                word = "rain";
                break;
            case "ветер":
                word = "wind";
                break;
            case "град":
                word = "hail";
                break;
            case "ливень":
                word = "shower";
                break;
            case "мороз":
                word = "freezing";
                break;
            case "облачно":
                word = "cloudy";
                break;
            case "гроза":
                word = "storm";
                break;
            case "уроган":
                word = "Hurricane";
                break;
            case "ясно":
                word = "clear";
                break;
            case "гололёд":
                word = "ice";
                break;

                default: Console.WriteLine("Unknown word");
                break;
        }
        Console.WriteLine(word);


    }
    }
}
